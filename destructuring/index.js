
function getNumbers() {

    var first = { a: 1 }
    var last = [ 3 ]

    var a = first.a
    var b = first.b === undefined ? 2 : first.b
    var c = last[0]
    var d = last[1] === undefined ? 4 : last[1]

    return `${a} ${b} ${c} ${d}`

}
