
function getText() {

    var callback = function() {
        document.getElementById('demo').innerHTML = "Finally done"
    }

    callReallySlowMethod(callback)
}

function callReallySlowMethod(callback) {
    setTimeout(function() {
        callback()
    }, 2000)
}
