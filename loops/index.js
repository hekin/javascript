
function calculateFor() {

    var list = getLargeList(10000000)

    const t0 = performance.now()

    for(let i=0; i<list.length; i++) {
        list[i] = list[i] + 1
    }

    const t1 = performance.now()

    document.getElementById('time').innerHTML = `This took ${t1 - t0} milliseconds.`
}

function getLargeList(numberOfElements) {
    return Array.from({length: numberOfElements}, () => Math.floor(Math.random() * numberOfElements));
}
