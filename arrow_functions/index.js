
function getEvenNumbers() {

    var odd = [1,3,5,7,9]

    var even = odd.map(function(number) {
        return number + 1
    })

    return even
}
