
function getCustomer() {

    var Customer = function(name, age) {
        this.name = name
        this.age = age
        this.getText(name, age)
    }

    Customer.prototype.getText = function(name, age) {
        return this.name + ", age: " + this.age
    }

    return new Customer('henrik', '36')
}
